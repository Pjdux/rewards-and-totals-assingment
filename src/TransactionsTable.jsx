import React, { useEffect, useState } from "react";

function TransactionsTable() {
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    fetch("transactionsRecord.json")
      .then((response) => response.json())
      .then((data) => setTransactions(data));
  }, []);

  const calculatePoints = (amount) => {
    let points = 0;

    if (amount > 100) {
      points += (amount - 100) * 2;
      points += 50; // additional points for the first $50 spent above $100
    } else if (amount > 50) {
      points += amount - 50;
    }

    return Math.round(points);
  };

  const calculateTotalPoints = (name) => {
    const userTransactions = transactions.filter(
      (transaction) => transaction.name === name
    );

    let totalPoints = 0;

    userTransactions.forEach((transaction) => {
      totalPoints += calculatePoints(transaction.transaction_amount);
    });

    return totalPoints;
  };

  return (
    <div className="transactions">
      <table striped="true" bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Transaction Amount</th>
            <th>Transaction Date</th>
            <th>Points Earned</th>
          </tr>
        </thead>
        <tbody>
          {transactions.map((transaction, index) => (
            <tr key={index}>
              <td>{transaction.name}</td>
              <td>&#36;{transaction.transaction_amount}</td>
              <td>{transaction.transaction_date}</td>
              <td>{calculatePoints(transaction.transaction_amount)}</td>
            </tr>
          ))}
        </tbody>
      
      </table>
<h3>All User Totals</h3>
      <div className="totals">
        <div className="single-total-container">
          <strong>Jimmy Page: </strong>
          {calculateTotalPoints("Jimmy Page")}
        </div>

        <div className="single-total-container">
          <strong>Paul McCartney: </strong>
          {calculateTotalPoints("Paul McCartney")}
        </div>

        <div className="single-total-container">
          <strong>Peter Grant: </strong>
          {calculateTotalPoints("Peter Grant")}
        </div>

        <div className="single-total-container">
          <strong>JC Diaz</strong>
          {calculateTotalPoints("JC Diaz")}
        </div>
      </div>
    </div>
  );
}

export default TransactionsTable;
